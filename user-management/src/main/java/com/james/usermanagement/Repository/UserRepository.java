package com.james.usermanagement.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.james.usermanagement.entity.UserData;

@Repository
public interface UserRepository extends JpaRepository<UserData, Integer> {

	public Optional<UserData> findByName(String userName);

}
