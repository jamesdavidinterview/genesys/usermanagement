package com.james.usermanagement.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.james.usermanagement.Repository.UserRepository;
import com.james.usermanagement.dto.LoginDTO;
import com.james.usermanagement.entity.UserData;

@Service
public class UserService implements IUserService {
	@Autowired
	UserRepository userRepo;

	@Override
	public List<UserData> getAllUsers() {
		return userRepo.findAll();
	}

	@Override
	public Optional<UserData> getUserById(int userId) {
		return userRepo.findById(userId);
	}

	@Override
	public Optional<UserData> getUserByName(String userName) {
		return userRepo.findByName(userName);
	}

	@Override
	public UserData createUser(UserData userData) {
		return userRepo.save(userData);
	}

	@Override
	public UserData updateUser(UserData userData) {
		return userRepo.save(userData);
	}

	@Override
	public boolean deleteUser(UserData userData) {
		Optional<UserData> optUser = getUserById(userData.getId());
		if (optUser.isPresent()) {
			userRepo.delete(userData);
			return true;
		} else {
			return false;
		}

	}

	@Override
	public boolean deleteUserById(int userId) {
		Optional<UserData> optUser = getUserById(userId);
		if (optUser.isPresent()) {
			userRepo.deleteById(userId);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public boolean authendicate(LoginDTO loginDto) {
		String loginUserName = loginDto.getName();
		String loginPassword = loginDto.getPassword();
		Optional<UserData> optUserData = getUserByName(loginUserName);
		if (optUserData.isPresent()) {
			if (loginPassword.equals(optUserData.get().getPassword())) {
				updateLoginInfo(optUserData.get());
				return true;
			}
		}
		return false;
	}

	private void updateLoginInfo(UserData userData) {
		userData.setLastLoginTime(new Date());
		updateUser(userData);
	}
}
