package com.james.usermanagement.service;

import java.util.List;
import java.util.Optional;

import com.james.usermanagement.dto.LoginDTO;
import com.james.usermanagement.entity.UserData;

public interface IUserService {

	List<UserData> getAllUsers();

	Optional<UserData> getUserById(int userId);

	Optional<UserData> getUserByName(String userName);

	UserData createUser(UserData userData);

	UserData updateUser(UserData userData);

	boolean deleteUser(UserData userData);

	boolean deleteUserById(int userId);

	boolean authendicate(LoginDTO loginDto);

}
