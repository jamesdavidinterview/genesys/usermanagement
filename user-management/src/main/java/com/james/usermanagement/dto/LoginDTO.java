package com.james.usermanagement.dto;

import lombok.Data;

@Data
public class LoginDTO {
	private String name;
	private String password;
}
