package com.james.usermanagement.controller;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.james.usermanagement.dto.LoginDTO;
import com.james.usermanagement.entity.UserData;
import com.james.usermanagement.service.IUserService;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	IUserService userService;

	@GetMapping("/fetch/all")
	public ResponseEntity<List<UserData>> getAllUsers() {
		return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
	}

	@GetMapping("/fetch/id/{userId}")
	public ResponseEntity<UserData> getUserById(@PathVariable int userId) {

		Optional<UserData> optUser = userService.getUserById(userId);
		if (optUser.isPresent()) {
			return new ResponseEntity<>(optUser.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

	@GetMapping("/fetch/name/{userName}")
	public ResponseEntity<UserData> getUserByName(@PathVariable String userName) {
		Optional<UserData> optUser = userService.getUserByName(userName);
		if (optUser.isPresent()) {
			return new ResponseEntity<>(optUser.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/create")
	public ResponseEntity<UserData> createUser(@RequestBody UserData userData) {
		UserData userObj = userService.createUser(userData);
		if (Objects.nonNull(userObj)) {
			return new ResponseEntity<>(userObj, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
		}
	}

	@PutMapping("/update")
	public ResponseEntity<UserData> updateUser(@RequestBody UserData userData) {
		UserData userObj = userService.updateUser(userData);
		if (Objects.nonNull(userObj)) {
			return new ResponseEntity<>(userObj, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_MODIFIED);
		}
	}

	@DeleteMapping("/delete")
	public ResponseEntity<UserData> deleteUser(@RequestBody UserData userData) {
		if (userService.deleteUser(userData)) {
			return new ResponseEntity<>(userData, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(userData, HttpStatus.NOT_FOUND);
		}
	}

	@DeleteMapping("/delete/{userId}")
	public ResponseEntity<UserData> deleteUserById(@PathVariable int userId) {
		if (userService.deleteUserById(userId)) {
			return new ResponseEntity<>(null, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

	@PostMapping("/login")
	public String authendicateUser(@RequestBody LoginDTO loginDto) {
		if (userService.authendicate(loginDto))
			return "Log in success for user : " + loginDto.getName();
		else
			return "Invalid Credintial";
	}

}
